Please copy .htpasswd_dummy. Then change content.

http://www.htaccesstools.com/htpasswd-generator/
```
cp .htpasswd_dummy .htpasswd
```

Activate lines in .htaccess_staging and edit path.

```
##########################################################################
#SetEnvIfNoCase Request_URI '/' SECURED=yes
#AuthType Basic
#AuthName "Staging"
#AuthUserFile /www/470890_78628/webseiten/<customer_abbr>/<type>/<yy>/staging/shared/web/.htpasswd
#Require valid-user
#Satisfy any
#Order allow,deny
#Allow from all
#Deny from env=SECURED
##########################################################################
```
