#!/bin/bash

echo ""
echo "HIVE | installer\n"

# get local stuff
echo "do local stuff"
cd /app/web/

if [ ! -f app.php ]; then
    echo "touch app.php"
    touch ./app.php
    echo "<?php phpinfo(); ?>" >> ./app.php
fi

if [ ! -f adminer.php ]; then
    echo "get adminer"
    wget https://www.adminer.org/latest.php -q -O ./adminer.php
fi


if [ ! -f .htaccess ]; then
    echo "ln -s .htaccess_local .htaccess"
    ln -s .htaccess_local .htaccess
fi

echo "\ncomposer install"
cd /app/
wget -q https://getcomposer.org/composer-stable.phar -O ./composer.phar
chmod 777 ./composer.phar && ./composer.phar install

echo "cd /app/web/typo3conf/ext/\n"

cd /app/web/typo3conf/ext/

echo "ln -s ../../../vendor/helhum/typo3-console typo3_console\n"

ln -s ../../../vendor/helhum/typo3-console typo3_console

echo "cd /app/\n"

cd /app/

echo "install typo3"

. $(dirname "$0")/web/typo3conf/local.ini

charset="$charset"
dbname="$dbname"
host="$host"
password="$password"
port="$port"
user="$user"
admin="$admin"
admin_password="$admin_password"
site_name="$site_name"
be_loginFootnote="$be_loginFootnote"
be_loginHighlightColor="$be_loginHighlightColor"
be_loginLogo="$be_loginLogo"
be_loginBackgroundImage="$be_loginBackgroundImage"
be_backendLogo="$be_backendLogo"
be_backendFavicon="$be_backendFavicon"
ext_availableLanguages="$ext_availableLanguages"

./web/typo3conf/ext/typo3_console/typo3cms install:setup \
    --no-interaction \
    --use-existing-database \
    --skip-extension-setup \
    --database-user-name="${user}" \
    --database-host-name="${host}" \
    --database-port="${port}" \
    --database-name="${dbname}" \
    --admin-user-name="${admin}" \
    --admin-password="${admin_password}" \
    --site-name="${site_name}"

echo ""
echo "Activate all extensions"
echo ""

./web/typo3conf/ext/typo3_console/typo3cms install:extensionsetupifpossible
./web/typo3conf/ext/typo3_console/typo3cms install:generatepackagestates

# fix typo3console and localconf when DB exist
hashed_pw=$(mysql dev -hmysql -udev --password="dev" -se "SELECT password FROM be_users WHERE username='sudo'")

./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'SYS/sitename' "${site_name}"
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'BE/installToolPassword' "${hashed_pw}"
chmod -R 777 ./var/
chmod 777 ./web/typo3conf/LocalConfiguration.php
chmod 777 ./web/typo3conf/PackageStates.php

# set BE Conf
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'EXTENSIONS/backend/backendFavicon' "${be_backendFavicon}"
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'EXTENSIONS/backend/backendLogo' "${be_backendLogo}"
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'EXTENSIONS/backend/loginFootnote' "${be_loginFootnote}"
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'EXTENSIONS/backend/loginHighlightColor' "${be_loginHighlightColor}"
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'EXTENSIONS/backend/loginLogo' "${be_loginLogo}"
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'EXTENSIONS/backend/loginBackgroundImage' "${be_loginBackgroundImage}"
./web/typo3conf/ext/typo3_console/typo3cms configuration:set 'EXTCONF/lang/availableLanguages' "[${ext_availableLanguages}]" --json

# update language
echo ""
echo "update language ..."
./vendor/bin/typo3 language:update
chmod -R 777 ./var/
chmod 777 ./web/typo3conf/LocalConfiguration.php
echo "done"
echo ""

echo ""
echo "User: ${admin}"
echo "Password: ${admin_password} (please change this by importing dummy db)"

echo ""
echo "Done."